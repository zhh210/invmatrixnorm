

%%% Local Variables: 
%%% Mode: Latex
%%% Tex-Master: T
%%% End: 
\documentclass[11pt]{article}
\usepackage{hyperref} % FEC: Added this package
\usepackage{amsfonts,amsthm,amssymb,algorithm,algorithmic}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{color}
%\usepackage{hyperref}
\newtheorem{thm}{Theorem}
\newtheorem{lemm}{Lemma}
\newtheorem{coro}{Corollary}
\newtheorem{fact}{Fact}
\newtheorem{prop}{Proposition}
\newtheorem{eg}{Example}
\usepackage{graphicx}
\usepackage[verbose,lmargin=0.5in,rmargin=0.5in,tmargin=0.5in]{geometry}
\usepackage{cite}
\usepackage{subfigure}
\usepackage[final]{changes} % FEC: Added this package
%\usepackage{showlabels}

\usepackage{xcolor}
\definecolor{dark-red}{rgb}{0.6,0.15,0.15}
\definecolor{dark-blue}{rgb}{0.15,0.15,0.8}
\definecolor{medium-blue}{rgb}{0,0,0.5}
\hypersetup{
    colorlinks, linkcolor={dark-red},
    citecolor={dark-blue}, urlcolor={medium-blue}
}
\begin{document}
\thispagestyle{empty} \pagenumbering{roman}
\title{\small Literature Review\\ \Large On computing the upper bound of inverse matrix norm}
\author{Zheng Han}
 \maketitle
% \tableofcontents
 \numberwithin{equation}{section}
% \newpage
 \begin{abstract}
   We collect useful results from linear algebra literature for computing the upper bound of an inverse matrix norm. Relevant topics include: generalized matrix norm theory, estimating the minimum singular value and condition number, matrix inequalities, etc..
 \end{abstract}

\setcounter{page}{1} 
\pagenumbering{arabic}

\section{Introduction}
\label{sec:introduction}
We introduce default assumptions and notations in this section. Let $\sigma$, $i=1,\ldots,n$, be the singular values of a nonsingular complex matrix $A$ of order $n$, such that  
\begin{equation*}
\sigma_1\geq\sigma_2\geq\cdots\geq\sigma_n>0    
\end{equation*}
The spectral norm is defined as $\kappa(A):=\frac{\sigma_1}{\sigma_n}$. $A\in\mathbb C^{n\times n}$ is called strictly diagonally dominant (SDD) if $|a_{ii}|>\sum_{j\neq i}|a_{ij}|$.

\section{Theoretical Bounds}
\label{sec:theoretical-bounds}

Guggenheimer, Edelman and Johnson \cite{Guggenheimer1995} give a simple upper bound of the spectral condition number of a nonsingular square matrix in terms of the determinant, the Frobenius norm.
 \begin{thm}
   For an arbitrary nonsingular $n\times n$ matrix $A$,
   \begin{equation}
     \label{eq:1}
     \kappa (A) < \frac{2}{|\mbox{det}(A)|}\left( \frac{\sum\sum a_{ij}^2}{n}\right)^{n/2} = \frac{2}{|\mbox{det}(A)|}\left( \frac{\mbox{tr} (A^*A)}{n}\right)^{n/2} = \frac{2}{|\mbox{det}(A)|}\left( \frac{\| A\|_F}{\sqrt{n}}\right)^{n}
   \end{equation}
 \end{thm}
Merikoski \cite{RefWorks:63} claimed to find a best upper bound for the spectral-norm condition number of a matrix in terms of the determinant, trance and size of $A$.
\begin{thm}[Theorem 2]
\begin{equation}
\kappa(A)  \leq \frac{1+\sqrt{1-(n/\mbox{tr}A)^n \mbox{det} A}}{1-\sqrt{1-(n/\mbox{tr}A)^n \mbox{det} A}}.
\end{equation}
The right-hand-side is the best possible upper bound of $\kappa(A)$ using $\mbox{tr}(A)$, $\mbox{det}(A)$, and $n$ only. Equality holds if and only if
\begin{equation*}
  \begin{aligned}
    \sigma_1 &= \frac{tr A}{n}\left[ 1 + \sqrt{1 -(\frac{n}{tr A})^n detA }\right],\\
    \sigma_n &= \frac{tr A}{n}\left[ 1 - \sqrt{1 -(\frac{n}{tr A})^n detA }\right],\\
    \sigma_2 &=\cdots=\sigma_n=\frac{tr A}{n}.
  \end{aligned}
\end{equation*}
\end{thm}

Piazza \cite{RefWorks:64} proposed  the bound of $\kappa(A)$ as well as $\sigma_{min}$ which also involves the determinant, trace and size of $A$. G\"ung\"or wrote an erratum to correct the bound of $\sigma_{min}$.
\begin{thm}
  \begin{equation}
    \begin{aligned}
      \kappa(A)&\leq \frac{2^k}{\displaystyle\prod_{i=2}^{n-1}\sigma_i}\frac{1}{|det A|}\left(
\frac{\| A\|_F}{\sqrt{n+k-1}}\right)^{n+k-1},\quad k=1,\ldots,n-1.\\
      \sigma_{min}&=\sigma_n \geq\left( \frac{n-1}{\|A\|_F^2} \right)^{(n-1)/2}|det A|.      
    \end{aligned}
  \end{equation}
\end{thm}

Bai et al \cite{Bai96} proposed bounds for $\mbox{tr}(A^{-1})$ and $\mbox{det}A$ for $A$ being a symmetric positive definite matrix. Their method arise from the fact that
\begin{equation*}
  \ln (det A)=tr(\ln(A)),
\end{equation*}
and unified the problem of bounding $\mbox{tr}(A^{-1})$ and $det(A)$ as bounding $tr(f(A))$ for $f(\lambda) = \lambda^{-1}$ and $\ln \lambda$ respectively. 
\begin{thm}
  Assume parameter $0 < \alpha \leq \sigma_n$ and $\sigma_1\leq\beta$, let $\mu_0=n$, $\mu_1=\displaystyle\sum_{i=1}^n a_{ii}$, and $\mu_2=\displaystyle\sum_{i,j=1}^n a_{ij}^2 =\| A\|_F^2$, then
  \begin{equation*}
    \begin{aligned}
      \begin{pmatrix}
        \mu_1 & n
      \end{pmatrix}
      \begin{pmatrix}
        \mu_2 & \mu_1\\
        \beta^2 & \beta
      \end{pmatrix}^{-1}
      \begin{pmatrix}
        n\\1
      \end{pmatrix}
      &\leq tr(A^{-1})&\leq&
      \begin{pmatrix}
        \mu_1 & n
      \end{pmatrix}
      \begin{pmatrix}
        \mu_2 & \mu_1\\
        \alpha^2 & \alpha
      \end{pmatrix}^{-1}
      \begin{pmatrix}
        n\\1
      \end{pmatrix},\\
      \begin{pmatrix}
        \ln \alpha & \ln \underline{t}
      \end{pmatrix}
      \begin{pmatrix}
        \alpha & \underline{t}\\
        \alpha^2 & \underline{t}^2
      \end{pmatrix}^{-1}
      \begin{pmatrix}
        \mu_1\\ \mu_2
      \end{pmatrix}
      &\leq tr(\ln(A))&\leq&
      \begin{pmatrix}
        \ln\beta & \ln\overline{t}
      \end{pmatrix}
      \begin{pmatrix}
        \beta & \overline t\\
        \beta^2 & \overline t
      \end{pmatrix}^{-1}
      \begin{pmatrix}
        \mu_1\\ \mu_2
      \end{pmatrix}.
    \end{aligned}
  \end{equation*}
where
\begin{equation*}
  \underline{t}=\frac{\alpha\mu_1 - \mu_2}{\alpha n -\mu_1},\mbox{  and }\overline t = \frac{\beta\mu_1 - \mu_2}{\beta n - \mu_1}.
\end{equation*}
\end{thm}

\section{Computable Bounds}
\label{sec:computable-bounds}
Dixon \cite{Dixon1983} proposed a probabilistic algorithm to make rough estimates of $\sigma_1$, $\sigma_n$, and $\kappa(A)$. Let $S_{n-1}:=\{x\in\mathbb R^n| x^Tx = 1\}$ denote the unit sphere.
\begin{thm}
  Let $A$ be a positive definite real matrix and let $\theta>1$. If $x$ is a random variable which is uniformly distributed over $S_{n-1}$ then the probability that the inequality
  \begin{equation*}
    x^TAx\leq\sigma_1\leq \theta x^TAx
  \end{equation*}
holds is at least $1-0.8\theta^{-k/2}n^{1/2}$.
\end{thm}
\begin{thm}
  Let $A\in\mathbb R^{n\times n}$ be a real nonsingular matrix and $k$ a positive integer. For all $x,y\in\mathbb R^n$ define
  \begin{equation*}
    \phi_k(x,y):= \{x^T(A^TA)^kx\cdot y^T(A^TA)^ky\}^{1/2k}.
  \end{equation*}
If $x$ and $y$ are random variables independently and uniformly distributed over $S_{n-1}$ and $\theta>1$, then the probability that
\begin{equation*}
  \phi_k(x,y) \leq \kappa(A) \leq \theta\phi_k(x,y)
\end{equation*}
holds is at least $1-1.6\theta^{-k/2}n^{1/2}$.
\end{thm}
Varah \cite{Varah19753} generalized lower and upper bounds of a SDD matrix on the inverse norm and smallest singular value when $A$ is strictly block diagonally dominant (SBDD). Consider matrix $A$ in block case:
\begin{equation*}
  \begin{pmatrix}
    A_{11} & A_{12} &\cdots & A_{1m}\\
    A_{21} & A_{22} &\cdots & A_{2m}\\
    \ldots& \ldots&\ddots &\ldots\\
    A_{m1} & A_{m2} &\cdots & A_{mm}
  \end{pmatrix},
\end{equation*}
$A$ is SBDD for the block if $\|A_{kk}^{-1}\|_\infty^{-1}-\sum_{j\neq k}\| A_{kj}\|_\infty)\geq 0$ for $k=1,\ldots,m$.
 The classic Ahlber-Nilson-Varah bound (1975) for block case comes as follows.
\begin{thm}
  Assume $A$ is block diagonally dominant by rows, and set $\alpha=\min_k(\|A_{kk}^{-1}\|_\infty^{-1}-\sum_{j\neq k}\| A_{kj}\|_\infty)$. Then $\| A^{-1}\|_\infty \leq 1/\alpha$.
\end{thm}
Notice that when $m=1$, $A$ is SDD and the above result is exactly the well known Ahlber-Nilson bound
\begin{equation*}
  \| A^{-1}\|_\infty \leq \frac{1}{\displaystyle\min_{i} a_{ii} - \sum_{i\neq j}|a_{ij}|}.
\end{equation*}

In \cite{Varga1976211}, Varga (1976) give an upper bound of $\|A^{-1}\|_\infty$ when $A$ is an $H$-matrix and a known vector $u$. Define
\begin{equation*}
  \begin{aligned}
    U_A&:=\{u>0:\ Au > 0\mbox{ and }\|u\|_\infty=1\},\\
    f_A(u)&:=\min_i\{(Au)_i\}>0\mbox{ for any }u\in U_A.
  \end{aligned}
\end{equation*}
\begin{thm}
  If $A\in\mathbb C^{n\times n}$ is a nonsingular $H$-matrix, then
  \begin{equation*}
    \|A\|_\infty \leq\frac{1}{\max\{f_A(u):\ u\in\overline{U_A}\}}.
  \end{equation*}
\end{thm}
It is worth noting that $\boldsymbol e\in\overline{U_A}$ if $A$ is SDD.

In \cite{Morača20082589}, Mora\v{s}ca listed many easily computable lower bounds for $\|A^{-1}\|_\infty$ and $\|A^{-1}\|_1$ for $A$ being an $M$-matrix. The author also derived the calculable upper bounds for $A$ being an SDD(strictly diagonally dominant) matrix. Bounding on the smallest singular value is also applied. 
\begin{thm}
  Let $A\in\mathbb C^{n\times n}, n\geq 2$, be SDD by columns. Then
  \begin{equation*}
    \| A^{-1}\|_\infty \leq \frac{n-\frac{\sum_j (|a_{jj}| - c_j(A)) -\min_j(|a_{jj}|-c_j(A))}{\max_i (|a_{ii}| - r_i(A))} } 
{\min_j (|a_{jj}|-c_j(A))}.
  \end{equation*}
where
\begin{equation*}
  \begin{aligned}
    r_i(A)&:=\sum_{j\neq i}|a_{ij}|,\\
    c_j(A)&:=\sum_{i\neq j}|a_{ij}|.
  \end{aligned}
\end{equation*}
\end{thm}
In \cite{Morača2007666} Mora\v{s}ca give upper bounds of $\|A^{-1}\|_\infty$ for $A$ being SDD and S-SDD.
\begin{thm}
  Let $A=[a_{ij}]\in\mathbb C^{n\times n}$, $n\geq 2$, be an S-SDD matrix for some none-empty proper subset $S$ of $N$ and let $\emptyset\subset T(A)\subset \bar S$, then
  \begin{equation*}
    \|A^{-1}\|_\infty\leq\max_{i\in S,j\in\bar S}\frac{|a_{ii}| - r_i^S(A)+r_j^{S}(A)}{(|a_{ii}|-r_i^S(A)) (|a_{jj}|-r_j^{\bar S}(A)) -r_i^{\bar S}(A)r_j^S(A)}.
  \end{equation*}
\end{thm}
where $T(A)$ is the index set of non-SDD rows of $A$.

Kolotilina \cite{Kolotilina2009692} proposed bounds for $\|A^{-1}\|_\infty$ when $A$ is certain $PM$- and $PH$- matrix.
\begin{thm}
  If $A\in\mathbb R^{n\times n}$, $n\geq 1$, is a $PM$-matrix with respect to a partition of the index into disjoint nonempty subsets, then $A$ is a nonsingular $M$-matrix, and its inverse satisfies
  \begin{equation*}
    \min_{i_1,\ldots,i_m}\|(A^{(i_1,\ldots,i_m)})^{-1} \|_\infty\leq \| A^{-1}\|_\infty \leq\max_{i_1,\ldots,i_m}\|(A^{(i_1,\ldots,i_m)})^{-1} \|_\infty.
  \end{equation*}
\end{thm}

Hillar \cite{2012arXiv1203.6812H} gives a tight bound for computing the infinity norm of the inverse of symetric, diagonally dominant positive matrices.
\begin{thm}
  Let $n\geq 3$. For any symmetric diagonally dominant matrix $A$ with $a_{ij}\geq\ell > 0$, we have
  \begin{equation*}
    \| A^{-1}\|_\infty \leq \frac{1}{\ell}\| S^{-1}\|_\infty=\frac{3n-4}{2\ell(n-2)(n-1)},
  \end{equation*}
where $S=(n-2)I_n +\boldsymbol 1_n \boldsymbol 1_n^T$. Moreover, equality is achieved if and only if $A = \ell S$.
\end{thm}


Volkov and Miroshnichenko \cite{Volkov2009} summarized estimates of $\|A^{-1}\|_\infty$ for $A$ being an $M$-matrix or totally positive matrix.


\begin{thm}\cite{horn1990matrix}
  Let $A\in \mathbb C^{n\times n}$ be nonsingular and suppose that a matrix norm $\|\cdot\|$ on $\mathbb C^{n\times n}$ is induced by the vector norm $\|\cdot\|$ on $\mathbb C^n$. Then,
  \begin{equation}\label{horn-ineq}
    \| A^{-1}\| = \frac{1}{\displaystyle\min_{\| x\| = 1} \|Ax \|}.
  \end{equation}

\end{thm}

% *** Need more thought on it ***
\begin{thm}
  (Under construction)Suppose $A\in\Re^{n\times n}$ is nonsingular, then $\| A^{-1} \|_1 \leq B$.
\end{thm}
\begin{proof}
For convenience, hereafter we omit the subscript and use $\|\cdot \|$ to represent $\|\cdot\|_1$. Next we explain how a lower bound of the denominator of \eqref{horn-ineq} is estimatable  by an LP:
\begin{equation}\label{lp-primal}
  \begin{aligned}
    \min_{x^+\in\Re^b, x^-\Re^n, s\in\Re^n}      &\; e^Ts\\
    \mbox{s.t.} &\; A(x^+ - x^-) \leq s,\\
    &\; -A(x^+ - x^-) \leq s,\\
    &\; e^T(x^+ + x^-) = 1,\\
    &\; x^+, x^- \geq 0.
  \end{aligned}
\end{equation}
The dual of \eqref{lp-primal} is
\begin{equation}
  \label{lp-dual}
  \begin{aligned}
    \max_{t^+,t^-\in\Re^n, \alpha\in\Re} &\; \alpha\\
    \mbox{s.t.} &\; -A^T(t^+-t^-) + \alpha e \leq 0,\\
    \mbox{s.t.} &\; A^T(t^+-t^-) + \alpha e \leq 0,\\
    &\; t^+ + t^- = e,\\
    &\; t^+, t^- \geq 0.
  \end{aligned}
\end{equation}
\end{proof}

Notice that $\| A^{-1}\|_\infty = \| A^{-T}\|_1$.

\bibliography{reference}
\bibliographystyle{plain}
\end{document}
